# Chatroom Server
* Aashish Arora, 16338462
* B.A.I Computer Engineering
* For CS7NS1 Scalable Computing module
* Last tested: 2017-11-23 13:22:19.806. Score: 93.

# Dependencies
This project requires no dependencies

# Deployment Instructions
1. Clone this repository using git clone https://aroraaa@bitbucket.org/aroraaa/chat-server-internet-application.git.
2. Run the 'compile.sh' script:
    * Assign execute permissions using 'chmod 755 compile.sh'. (don't forget 'sudo' if necessary).
    * Run using './compile.sh'. 
3. Run the 'start.sh' script:
    * As before, assign execute permissions using 'chmod 755 start.sh'.
    * Run the script, specifying the port number you want the server to be accessible at. This looks something like
     './start.sh <port_number>'.
    * You should see a message similar to the following:
		"Server started successfully"
   
# Remote Debugging
* In the 'start.sh' script, comment out the (1) line and uncomment the (2) line to make remote debugging available to an IDE. The default port is set to be '23456', and the server will suspend startup until a debugger attaches.
* Important: Do not have both (1) and (2) uncommented at the same time.

# Extra Notes
* The architecture has not been changed the initial commit.