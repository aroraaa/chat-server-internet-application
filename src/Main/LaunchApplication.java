
package Main;

import java.io.IOException;

public class LaunchApplication
{
    public static void main(String args[]){
        try{
            Server server = new Server();
            server.createServer(Integer.parseInt(args[0]));
            System.out.println("Server started successfully at "+Server.server_ip);
            server.acceptClientConnection(server.getServerSocket(), Integer.parseInt(args[0]));
        }
        catch(Exception e){
           System.out.println("Error occured while connecting the server");
           e.printStackTrace();
        }
        
    }
}
