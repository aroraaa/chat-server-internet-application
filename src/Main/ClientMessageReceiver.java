package Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientMessageReceiver extends Thread{
        
    Socket socket;
        
    ClientMessageReceiver(Socket socket) throws IOException{
        this.socket = socket;
    }
        
    @Override
    public void run(){
        printChat();
        
    }
        
    public void printChat(){
        try{
            String message;
            BufferedReader client_input_stream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while(true){
                message = client_input_stream.readLine();
                System.out.println(message);
            }
        }
        catch(Exception e){
            System.out.println("Error occured in Client Message Receiver");
        }
    }
}