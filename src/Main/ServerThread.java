package Main;

import Main.Chatroom;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerThread extends Thread{
    Socket socket = null;
    PrintWriter socket_output_stream = null;
    BufferedReader socket_input_stream = null;
    int port_number;
    List<String> joined_chatrooms_list = new ArrayList<String>();
    
    ServerThread(Socket socket, int port_number) throws IOException{
        this.socket = socket;
        this.port_number = port_number;
        socket_output_stream = Server.getServerSocketOutputStream(socket);
        socket_input_stream = Server.getServerSocketInputStream(socket);
    }
    
    public void run(){
        while(true){
            String message;
            try{
                message = readClientMessages();
                responseToClientMessage(message, port_number);
            } 
            catch (IOException ex){
            }
        }
    }
    
    public String readClientMessages() throws IOException{
        String message;
        message = socket_input_stream.readLine();
        System.out.println(message);
        return message;
    }
        
    public String responseToClientMessage(String message, int port_number) throws IOException{
        String error_message = null;
        String response = null;
        String first_word_of_message = getFirstWordOfString(message);
        if(first_word_of_message != null){
            if(first_word_of_message.equals("HELO")){
                String helo_text = getSecondWordOfString(message);
                if(helo_text == null){
                    error_message = "The text for HELO request is not mentioned";
                    response = getErrorHandler(error_message);
                }
                else{
                    response = getHeloResponse(message);
                    if(response.equals("Error")){
                        error_message = "Error occured while responding to the HELO request";
                        response = getErrorHandler(error_message);
                    }
                }
            }
            else if(first_word_of_message.equals("JOIN_CHATROOM:")){
                String join_chatroom_name = getSecondWordOfString(message);
                if(join_chatroom_name == null){
                    error_message = "Chatroom name not defined";
                    response = getErrorHandler(error_message);
                }
                else{
                    List<String> join_chatroom_request = getJoinChatroomRequest();
                    if(join_chatroom_request == null){
                        error_message = "Wrong format to join chatroom";
                        response = getErrorHandler(error_message);
                    }
                    else{
                        String client_name = getClientNameFromJoinChatroomRequest(join_chatroom_request);
                        if(client_name == null){
                            error_message = "Client name is not defined";
                            response = getErrorHandler(error_message);
                        }
                        else{
                           response = getJoinChatroomResponse(message, client_name);
                            if(response.equals("Error")){
                                error_message = "Error occured while responding to the JOIN_CHATROOM request";
                                response = getErrorHandler(error_message);
                            }
                            else{
                                String chatroom_name = getSecondWordOfString(response);
                                if(chatroom_name == null){
                                    error_message = "Error occured while getting the chatroom name";
                                    response = getErrorHandler(error_message);
                                }
                                else{
                                    response = getSendResponse(response);
                                    if(response != null){
                                        error_message = "Error occured while responding to join chatroom request";
                                        response = getErrorHandler(error_message);
                                    }
                                    else{
                                       String new_client_joined_message_to_chatroom = client_name + " has joined this chatroom";
                                        String broadcasting_response = broadcastMessageInChatroom(new_client_joined_message_to_chatroom, chatroom_name, client_name);
                                        if(broadcasting_response == null){
                                            error_message = "Error occured while broadcasting message";
                                            response = getErrorHandler(error_message);
                                        }
                                        else{
                                            String add_chatroom_to_joined_chatroom_list_response = addChatroomToJoinedChatroomList(chatroom_name);
                                            if(add_chatroom_to_joined_chatroom_list_response == null){
                                                error_message = "Error occured while adding chatroom to the joined chatroom list";
                                                response = getErrorHandler(error_message);
                                            }
                                        } 
                                    }
                                }
                            } 
                        }
                    }
                }
            }
            else if(first_word_of_message.equals("LEAVE_CHATROOM:")){
                String leave_chatroom_ref = getSecondWordOfString(message);
                if(leave_chatroom_ref == null){
                    error_message = "Chatroom name not defined";
                    response = getErrorHandler(error_message);
                }
                else{
                   List<String> leave_chatroom_request = getLeaveChatroomRequest();
                    if(leave_chatroom_request == null){
                        error_message = "Wrong format to leave chatroom";
                        response = getErrorHandler(error_message);
                    }
                    else{
                        String client_name = getClientNameFromLeaveChatroomRequest(leave_chatroom_request);
                        if(client_name == null){
                            error_message = "Client name is not defined";
                            response = getErrorHandler(error_message);
                        }
                        else{
                            response = getLeaveChatroomResponse(leave_chatroom_ref, leave_chatroom_request);
                            if(response == null){
                                error_message = "Error occured while leaving chatroom";
                                response = getErrorHandler(error_message);
                            }
                            else{
                                String chatroom_name = getChatroomName(leave_chatroom_ref);
                                if(chatroom_name == null){
                                    error_message = "Error occured while getting chatroom name";
                                    response = getErrorHandler(error_message);
                                }
                                else{
                                    response = getSendResponse(response);
                                    if(response != null){
                                        error_message = "Error occured while responding to leave chatroom request";
                                        response = getErrorHandler(error_message);
                                    }
                                    else{
                                        String client_left_chatroom_message_to_chatroom = client_name + " has left this chatroom";
                                        String broadcasting_response = broadcastMessageInChatroom(client_left_chatroom_message_to_chatroom, chatroom_name, client_name);
                                        if(broadcasting_response == null){
                                                error_message = "Error occured while broadcasting message";
                                                response = getErrorHandler(error_message);
                                        }
                                        else{
                                            String remove_chatroom_from_joined_chatroom_list_response = removeChatroomFromJoinedChatroomsList(chatroom_name);
                                            if(remove_chatroom_from_joined_chatroom_list_response == null){
                                                error_message = "Error occured while removing chatroom from the joined chatroom list";
                                                response = getErrorHandler(error_message);
                                            }
                                            else{
                                                response = getLeaveChatroom(leave_chatroom_ref);
                                                if(response == null){
                                                    error_message = "Error occured while leaving chatroom";
                                                    response = getErrorHandler(error_message);
                                                }
                                                else{
                                                    response = null;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(first_word_of_message.equals("DISCONNECT:")){
                String ip_of_client = getSecondWordOfString(message);
                if(!ip_of_client.equals("0")){
                    error_message = "Defined IP is incorrect";
                    response = getErrorHandler(error_message);
                }
                else{
                    List<String> disconnect_request = getDisconnectRequest();
                    if(disconnect_request == null){
                        error_message = "Wrong format to leave chatroom";
                        response = getErrorHandler(error_message);
                    }
                    else{
                        String client_name = getClientNameFromDisconnectRequest(disconnect_request);
                        if(client_name == null){
                            error_message = "Client name is not defined";
                            response = getErrorHandler(error_message);
                        }
                        else{
                            String client_left_chatroom_message_to_chatroom = client_name + " has left this chatroom";
                            String broadcasting_response = broadcastMessageToAllJoinedChatroom(client_left_chatroom_message_to_chatroom, client_name);
                            if(broadcasting_response == null){
                                error_message = "Error occured while broadcasting message";
                                response = getErrorHandler(error_message);
                            }
                            else{
                                String remove_client_from_all_chatrooms_response = removeClientFromAllChatrooms();
                                if(remove_client_from_all_chatrooms_response == null){
                                    error_message = "Error occured while removing client from chatrooms";
                                    response = getErrorHandler(error_message);
                                }
                                else{
                                    String close_server_thread_response = closeServerThread();
                                    if(close_server_thread_response == null){
                                        error_message = "Error occured while closing server thread";
                                        response = getErrorHandler(error_message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(first_word_of_message.equals("CHAT:")){
                String chatroom_ref = getSecondWordOfString(message);
                if(chatroom_ref == null){
                    error_message = "Chatroom number not defined";
                    response = getErrorHandler(error_message);
                }
                else{
                   List<String> send_message_request = getSendMessageRequest();
                    if(send_message_request == null){
                        error_message = "Wrong format to send a message to the chatroom";
                        response = getErrorHandler(error_message);
                    }
                    else{
                        String client_name = getClientNameFromSendMessageRequest(send_message_request);
                        if(client_name == null){
                            error_message = "Client name is not defined";
                            response = getErrorHandler(error_message);
                        }
                        else{
                            String send_message_to_chatroom = getMessageFromSendMessageRequest(send_message_request.get(2));
                            if(send_message_to_chatroom == null){
                                error_message = "Error occured while getting message from send message request";
                                response = getErrorHandler(error_message);
                            }
                            else{
                                String chatroom_name = getChatroomName(chatroom_ref);
                                if(chatroom_name == null){
                                    error_message = "Error occured while getting chatroom name";
                                    response = getErrorHandler(error_message);
                                }
                                else{
                                    String broadcasting_response = broadcastMessageInChatroom(send_message_to_chatroom, chatroom_name, client_name);
                                    if(broadcasting_response == null){
                                            error_message = "Error occured while broadcasting message";
                                            response = getErrorHandler(error_message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(first_word_of_message.equals("KILL_SERVICE")){
                response = Server.getStopAllConnection();
                if(response == null){
                    error_message = "Error occured while closing all the connections";
                    response = getErrorHandler(error_message);
                }
                else{
                    response = null;
                }
            }
            else{
                response = "UNKOWN REQUEST";
            }
        }
        if(response != null){
            socket_output_stream.println(response);
            System.out.println(response);
        }
        return response;
    }
    
    public String getSendResponse(String message){
        try{
            socket_output_stream.println(message);
            System.out.println(message);
            return null;
        }
        catch(Exception e){
            return "Error";
        }
    }
    
    public String getHeloResponse(String message){
        String response = null;
        try{
            response = message + "\nIP: 10.62.0.74\nPort: " + port_number + "\nStudentID: 16338462";
        }
        catch(Exception e){
            response = "Error";
        }
        return response;
    }
    
    public String getJoinChatroomResponse(String message, String client_name){
        String response;
        String chatroom_name = getSecondWordOfString(message);
        if(chatroom_name == null){
            response = "Error";
        }
        else{
            Chatroom chatroom = getChatroom(chatroom_name);
            if(chatroom == null){
                response = "Error";
            }
            else{
                int chatroom_ref = getChatroomRef(chatroom_name);
                if(chatroom_ref == -1){
                    response = "Error";
                }
                else{
                    int join_id = getJoinId(client_name, chatroom, socket);
                    if(join_id == -1){
                        response = "Error";
                    }
                    else{
                        response = "JOINED_CHATROOM: " + chatroom_name + " \nSERVER_IP: " + Server.server_ip + " \nPORT: " + port_number + " \nROOM_REF: " + chatroom_ref + " \nJOIN_ID: " + join_id;
                    }
                }
            }
        }
        return response;
    }
    
    public String getLeaveChatroomResponse(String leave_chatroom_ref, List<String> leave_chatroom_request){
        String response;
        try{
            response = "LEFT_CHATROOM: " + leave_chatroom_ref + "\n" + leave_chatroom_request.get(0);
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public int getCheckChatroom(String name_of_chatroom){
        int check_chatroom = -1;
        try{
            for(int i = 0; i < Server.chatroom_list.size(); i++){
                if(Server.chatroom_list.get(i).chatroom_name.equals(name_of_chatroom)){
                    check_chatroom = i;
                    break;
                }
            }   
        }
        catch(Exception e){
            check_chatroom = -1;
        }
        return check_chatroom;
    }
    
    public Chatroom getChatroom(String chatroom_name){
        Chatroom chatroom = null;
        int check_chatroom = getCheckChatroom(chatroom_name);
        try{
            if(check_chatroom == -1){
            System.out.println("Creating chatroom: " + chatroom_name);
            List<Socket> sockets_list = new ArrayList<Socket>();
            chatroom = new Chatroom(chatroom_name, sockets_list);
            System.out.println("Chatroom: " + chatroom_name + " created successfully");
            Server.chatroom_list.add(chatroom);
        }
            else{
                System.out.println("Chatroom: " + chatroom_name + " already exist");
                chatroom = Server.chatroom_list.get(check_chatroom);
            }
        }
        catch(Exception e){
            chatroom = null;
        }
        return chatroom;
    }
    
    public int getChatroomRef(String chatroom_name){
        int chatroom_ref = -1;
        try{
            for(int i = 0; i < Server.chatroom_list.size(); i++){
                if(Server.chatroom_list.get(i).chatroom_name.equals(chatroom_name)){
                    chatroom_ref = i;
                }
            }
        }
        catch(Exception e){
            chatroom_ref = -1;
        }
        return chatroom_ref;
    }
    
    public String getChatroomName(String chatroom_ref){
        String chatroom_name = null;
        try{
            chatroom_name = Server.chatroom_list.get(Integer.parseInt(chatroom_ref)).chatroom_name;
        }
        catch(Exception e){
            chatroom_name = null;
        }
        return chatroom_name;
    }
    
    public int getJoinId(String client_name, Chatroom chatroom, Socket socket){
        int join_id = -1;
        try{
            for(int i = 0; i < chatroom.sockets_list.size(); i++){
                if(chatroom.sockets_list.get(i) == socket){
                    join_id = i;
                    System.out.println(client_name +" is already a member of the chatroom");
                    break;
                }
            }
            if(join_id == -1){
                System.out.println("Adding " + client_name + " to chatroom");
                chatroom.sockets_list.add(socket);
                System.out.println(client_name +" added successfully to the chatroom");
                join_id = chatroom.sockets_list.size() - 1;
            }
        }
        catch(Exception e){
            join_id = -1;  
        }
        return join_id;
    }
        
    public List<String> getJoinChatroomRequest(){
        List<String> join_chatroom_request_list = new ArrayList<String>();
        try{
            String read_line = readClientMessages();
            if(getFirstWordOfString(read_line).equals("CLIENT_IP:")){
                if(getSecondWordOfString(read_line).equals("0")){
                    join_chatroom_request_list.add(read_line);

                    read_line = readClientMessages();
                    if(getFirstWordOfString(read_line).equals("PORT:")){
                        if(getSecondWordOfString(read_line).equals("0")){
                            join_chatroom_request_list.add(read_line);

                            read_line = readClientMessages();
                            if(getFirstWordOfString(read_line).equals("CLIENT_NAME:")){
                                join_chatroom_request_list.add(read_line);
                            }
                            else{
                                join_chatroom_request_list = null;
                            }
                        }
                        else{
                            join_chatroom_request_list = null;
                        }
                    }
                    else{
                        join_chatroom_request_list = null;
                    }
                }
                else{
                    join_chatroom_request_list = null;
                }
            }
            else{
                join_chatroom_request_list = null;
            }
        }
        catch(Exception e){
            join_chatroom_request_list = null;
        }
        return join_chatroom_request_list;
    }
    
    public List<String> getLeaveChatroomRequest(){
        List<String> leave_chatroom_request_list = new ArrayList<String>();
        try{
            String read_line = readClientMessages();
            if(getFirstWordOfString(read_line).equals("JOIN_ID:")){
                if(getSecondWordOfString(read_line) != null){
                    leave_chatroom_request_list.add(read_line);

                    read_line = readClientMessages();
                    if(getFirstWordOfString(read_line).equals("CLIENT_NAME:")){
                        if(getSecondWordOfString(read_line) != null){
                            leave_chatroom_request_list.add(read_line);
                        }
                        else{
                            leave_chatroom_request_list = null;
                        }
                    }
                    else{
                        leave_chatroom_request_list = null;
                    }
                }
                else{
                    leave_chatroom_request_list = null;
                }
            }
            else{
                leave_chatroom_request_list = null;
            }
        }
        catch(Exception e){
            leave_chatroom_request_list = null;
        }
        return leave_chatroom_request_list;
    }
    
    public List<String> getDisconnectRequest(){
        List<String> disconnect_request_list = new ArrayList<String>();
        try{
            String read_line = readClientMessages();
            if(getFirstWordOfString(read_line).equals("PORT:")){
                if(getSecondWordOfString(read_line).equals("0")){
                    disconnect_request_list.add(read_line);

                    read_line = readClientMessages();
                    if(getFirstWordOfString(read_line).equals("CLIENT_NAME:")){
                        if(getSecondWordOfString(read_line) != null){
                            disconnect_request_list.add(read_line);
                        }
                        else{
                            disconnect_request_list = null;
                        }
                    }
                    else{
                        disconnect_request_list = null;
                    }
                }
                else{
                    disconnect_request_list = null;
                }
            }
            else{
                disconnect_request_list = null;
            }
        }
        catch(Exception e){
            disconnect_request_list = null;
        }
        return disconnect_request_list;
    }
    
    public List<String> getSendMessageRequest(){
        List<String> send_message_request_list = new ArrayList<String>();
        try{
            String read_line = readClientMessages();
            if(getFirstWordOfString(read_line).equals("JOIN_ID:")){
                if(getSecondWordOfString(read_line) != null){
                    send_message_request_list.add(read_line);

                    read_line = readClientMessages();
                    if(getFirstWordOfString(read_line).equals("CLIENT_NAME:")){
                        if(getSecondWordOfString(read_line) != null){
                            send_message_request_list.add(read_line);

                            read_line = readClientMessages();
                            if(getFirstWordOfString(read_line).equals("MESSAGE:")){
                                if(getSecondWordOfString(read_line) != null){
                                    send_message_request_list.add(read_line);
                                }    
                            }
                            else{
                                send_message_request_list = null;
                            }
                        }
                        else{
                            send_message_request_list = null;
                        }
                    }
                    else{
                        send_message_request_list = null;
                    }
                }
                else{
                    send_message_request_list = null;
                }
            }
            else{
                send_message_request_list = null;
            }
        }
        catch(Exception e){
            send_message_request_list = null;
        }
        return send_message_request_list;
    }
    
    public String getClientNameFromJoinChatroomRequest(List<String> join_chatroom_list){
        String client_name = null;
        try{
            client_name = getSecondWordOfString(join_chatroom_list.get(2));
        }
        catch(Exception e){
            client_name = null;
        }
        return client_name;
    }
    
    public String getClientNameFromLeaveChatroomRequest(List<String> leave_chatroom_list){
        String client_name = null;
        try{
            client_name = getSecondWordOfString(leave_chatroom_list.get(1));
        }
        catch(Exception e){
            client_name = null;
        }
        return client_name;
    }        
    
    public String getClientNameFromDisconnectRequest(List<String> disconnect_list){
        String client_name = null;
        try{
            client_name = getSecondWordOfString(disconnect_list.get(1));
        }
        catch(Exception e){
            client_name = null;
        }
        return client_name;
    }
    
    public String getClientNameFromSendMessageRequest(List<String> send_message_request){
        String client_name = null;
        try{
            client_name = getSecondWordOfString(send_message_request.get(1));
        }
        catch(Exception e){
            client_name = null;
        }
        return client_name;
    }
    
    public String getLeaveChatroom(String chatroom_ref){
        String left_chatroom_response;
        try{
            int leave_chatroom_ref = Integer.parseInt(chatroom_ref);
            if(leave_chatroom_ref < Server.chatroom_list.size()){
                String remove_client = getRemoveClientFromChatroom(leave_chatroom_ref);
                if(remove_client == null){
                    left_chatroom_response = null;
                }
                else{
                    left_chatroom_response = "Client Removed";
                }
            }
            else{
                left_chatroom_response = null;
            }
        }
        catch(Exception e){
            left_chatroom_response = null;
        }
        return left_chatroom_response;
    }
    
    public String getRemoveClientFromChatroom(int chatroom_ref){
        String remove_client;
        try{
            Server.chatroom_list.get(chatroom_ref).sockets_list.remove(socket);
            remove_client = "Client Removed";
        }
        catch(Exception e){
            remove_client = null;
        }
        return remove_client;
    }
    
    public String addChatroomToJoinedChatroomList(String chatroom_name){
        String response = null;
        try{
            joined_chatrooms_list.add(chatroom_name);
            response = "Chatroom added to the list";
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String removeChatroomFromJoinedChatroomsList(String chatroom_name){
        String response = null;
        try{
            joined_chatrooms_list.remove(chatroom_name);
            response = "Chatroom removed from the list";
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String removeClientFromAllChatrooms(){
        String response = null;
        try{
            for(int i = 0; i < joined_chatrooms_list.size(); i++){
                int chatroom_ref = getChatroomRef(joined_chatrooms_list.get(i));
                Chatroom chatroom = Server.chatroom_list.get(chatroom_ref);
                chatroom.sockets_list.remove(socket);
            }
            response = "Client removed from all the chatrooms he joined";
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String broadcastMessageInChatroom(String message, String chatroom_name, String sender_name){
        String response = null;
        try{
            int chatroom_ref = getChatroomRef(chatroom_name);
            if(chatroom_ref != -1){
                message = "CHAT: " + chatroom_ref + " \nCLIENT_NAME: " + sender_name + " \nMESSAGE: " + message + "\n";
                PrintWriter sockets_output_stream = null;
                Chatroom chatroom = Server.chatroom_list.get(chatroom_ref);
                for(int i = 0; i < chatroom.sockets_list.size(); i++){
                    sockets_output_stream = Server.getServerSocketOutputStream(chatroom.sockets_list.get(i));
                    sockets_output_stream.println(message);
                }
                response = "Message broadcasted";
            }
            else{
                response = null;
            }
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String broadcastMessageToAllJoinedChatroom(String message, String sender_name){
        String response = null;
        try{
            PrintWriter sockets_output_stream = null;
            for(int i = 0; i < joined_chatrooms_list.size(); i++){
                String message_to_send = null;                
                int chatroom_ref = getChatroomRef(joined_chatrooms_list.get(i));
                Chatroom chatroom = Server.chatroom_list.get(chatroom_ref);
                message_to_send = "CHAT: " + chatroom_ref + " \nCLIENT_NAME: " + sender_name + " \nMESSAGE: " + message + "\n";
                for(int j = 0; j < chatroom.sockets_list.size(); j++){
                    sockets_output_stream = Server.getServerSocketOutputStream(chatroom.sockets_list.get(j));
                    sockets_output_stream.println(message_to_send);
                }
            }
            response = "Message broadcasted";
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String closeServerThread(){
        String response = null;
        try{
            socket_output_stream.close();
            socket_input_stream.close();
            socket.close();
            response = "Server Thread closed";
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public String getFirstWordOfString(String message){
        String first_word_of_string;
        if(message.isEmpty()){
            return null;
        }
        else{
            first_word_of_string = message.split(" ")[0];
            return first_word_of_string;
        }
    }
    
    public String getSecondWordOfString(String message){
        String second_word_of_string = null;
        try{
            if(message.length() != 0)
            second_word_of_string = message.split(" ")[1];
        }
        catch(Exception e){
            second_word_of_string = null;
        }
        return second_word_of_string;        
    }
    
   public String getMessageFromSendMessageRequest(String message){
       String message_from_send_message_request = null;
        try{
            if(message.length() != 0)
            message_from_send_message_request = message.split(" ", 2)[1];
            
        }
        catch(Exception e){
            message_from_send_message_request = null;
        }
        return message_from_send_message_request;    
   }
    
    public String getErrorHandler(String error_message){
        String response = "ERROR_CODE: " + Server.error_code +" \nERROR_DESCRIPTION: " + error_message;
        Server.error_code++;
        return response;
    }
}
