package Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server{
    
    public static List<Socket> server_socket_list = new ArrayList<Socket>();
    public static List<Chatroom> chatroom_list = new ArrayList<Chatroom>();
    public static ServerSocket server_socket;
    public static int error_code = 0;
    public static String server_ip = null;
    public static boolean listen_to_connections = true;

      
    public String createServer(int port_number) throws IOException{
        server_socket = new ServerSocket(port_number);
        server_ip = InetAddress.getLocalHost().getHostAddress().toString();
        return "Server created successfully";
    }
    
    public ServerSocket getServerSocket(){
        return server_socket;
    }
        
    public void acceptClientConnection(ServerSocket server_sockets, int port_number) throws IOException{
        Socket socket = null;
       while(listen_to_connections){
    	   try{
    		socket = server_sockets.accept();
                System.out.println("Client Connected");
                addServerSocket(socket);
                runServerThread(socket, port_number);
            }
            catch(Exception e){
                server_socket.close();
            }
        }
    }
    
    public void addServerSocket(Socket socket){
        server_socket_list.add(socket);
    }
    
    public static String getStopAllConnection(){
        String response = null;
        try{
            for(int i = 0; i < server_socket_list.size(); i++){
                Socket socket = server_socket_list.get(i);
                socket.close();
            }
            response = "All sockets closed";
            listen_to_connections = false;
            server_socket.close();
        }
        catch(Exception e){
            response = null;
        }
        return response;
    }
    
    public void runServerThread(Socket socket, int port_number) throws IOException{
        if(socket != null){
            ServerThread server_thread = new ServerThread(socket, port_number);
            server_thread.start();
        }
    }
    
    public static BufferedReader getServerSocketInputStream(Socket socket) throws IOException{
        BufferedReader server_socket_input_stream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return server_socket_input_stream;
    }
    
    public static PrintWriter getServerSocketOutputStream(Socket socket) throws IOException{
        PrintWriter server_socket_output_stream = new PrintWriter(socket.getOutputStream(), true);
        return server_socket_output_stream;
    }
}
