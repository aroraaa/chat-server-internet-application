package Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientMessageSender extends Thread{
        
        Socket socket;
        
        ClientMessageSender(Socket socket) throws IOException{
            this.socket = socket;
        }
        
        @Override
        public void run()
        {
            writeMessageAtChatroom();
        }
        
        public void writeMessageAtChatroom(){
            try{
                PrintWriter client_output_stream_writer = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader client_system_in = new BufferedReader(new InputStreamReader(System.in));
                while(true){
                    String message = client_system_in.readLine();
                    client_output_stream_writer.println(message);
                    client_output_stream_writer.flush();
                }
            }
            catch(IOException e){
                System.out.println("Error occured in Client Message Sender");
            }
            
        }
    }