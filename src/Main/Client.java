package Main;

import java.io.IOException;
import java.net.Socket;

public class Client{
       
    public static Socket socket;
        
    public static void createClientSocket(int port_number) throws IOException{
        socket = new Socket("localhost", port_number);
    } 
    
    public static Socket getClientSocket(){
         return socket;
     }
    
    public static void runClientThreads(Socket socket) throws IOException{
        ClientMessageSender send_message_to_server = new ClientMessageSender(socket);
        ClientMessageReceiver receive_message_from_server = new ClientMessageReceiver(socket);
        send_message_to_server.start();
        receive_message_from_server.start();
    }
}
