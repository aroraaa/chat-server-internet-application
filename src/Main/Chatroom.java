package Main;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Chatroom{
    List<Socket> sockets_list = new ArrayList<Socket>();
    String chatroom_name;
     
    Chatroom(String chatroom_name, List<Socket> sockets_list){
        this.chatroom_name = chatroom_name;
        this.sockets_list = sockets_list;
    }
    
}
