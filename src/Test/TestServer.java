package Test;

import Main.Server;
import java.io.IOException;

public class TestServer{
    
    public static void testCreateSrever() throws IOException{
        Server server = new Server();
        String message = server.createServer(1000);
        if(message.equals("Server created successfully")){
            System.out.println("Passed");
        }
        else{
            System.out.println("Failed");
        }
    }
    
    public static void testClient() throws IOException{
        Server server = new Server();
        server.createServer(1000);
        server.acceptClientConnection(server.getServerSocket(), 1000);
    }
    
    public static void testClientResquestForHELO() throws IOException{
        Server server = new Server();
        server.createServer(1000);
        server.acceptClientConnection(server.getServerSocket(), 1000);
    }
    
    public static void main(String args[]) throws IOException{
        TestServer test_server = new TestServer();
        test_server.testClient();
    }
}

