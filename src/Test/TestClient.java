package Test;

import Main.Client;
import java.io.IOException;

public class TestClient{
    
    public static void testClientSocket() throws IOException{
        Client.createClientSocket(1000);
        Client.runClientThreads(Client.getClientSocket());
    }
    
    public static void testClientResquestForHELO() throws IOException{
        Client.createClientSocket(1000);
        Client.runClientThreads(Client.getClientSocket());
    }
    
    public static void main(String args[]) throws IOException{
        TestClient.testClientResquestForHELO();
    }
}
